'use strict';
const mongoose = require('mongoose');
const bodyparser = require('body-parser');
const express = require('express');
const path = require('path');
const serverless = require('serverless-http');

require('dotenv').config(); 

const router = require('./routes/router');

const app = express();

// Connection to MongoDB
const mongoUrl = `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASS}@${process.env.MONGO_HOST}/checker-registry?retryWrites=true&w=majority`;

// mongoose.connect(mongoUrl,
//     {
//         useNewUrlParser: true,
//         useUnifiedTopology: true
//     }).then(() => console.log('Connection to MongoDB established'))
//     .catch(err => console.log(`Error connecting to MongoDB: ${err}`));

// route definition
app.use(bodyparser.json());
app.use('/.netlify/functions/server', router);  // path must route to lambda
app.use('/', (req, res) => res.sendFile(path.join(__dirname, '../index.html')));


// Server running
app.listen(3000, () => console.log('Server started on port 3000'));
serverless(app);

