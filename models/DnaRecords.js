const mongoose = require('mongoose');
const { Schema } = require('mongoose');

const DnaRecordSchema = new Schema({
    sequence_dna: { type: String, required: true },
    has_mutation: { type: Boolean, required: true }
})

const DnaRecord = mongoose.model('dna-records', DnaRecordSchema)

module.exports = DnaRecord;