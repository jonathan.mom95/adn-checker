// const DnaRecords = require('../models/DnaRecords')


async function getRecordsStats(_, res){
  //   const countMutations = await DnaRecords.countDocuments({ has_mutation: true });
  // const countNoMutations = await DnaRecords.countDocuments({ has_mutation: false });
  const countMutations = 10;
  const countNoMutations =15;
  const totalSequences = countMutations + countNoMutations;
  const ratio = countMutations / totalSequences;
  res.json({ count_mutations: countMutations, count_no_mutation: countNoMutations, ratio: ratio });
}

module.exports = getRecordsStats;