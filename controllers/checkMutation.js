// const DnaRecords = require('../models/DnaRecords')
const hasMutation = require('../hasMutation')

async function checkMutation(req, res) {
    const dnaBody = req.body.dna;

    // Check for mutations in the DNA sequence
    const isMutated = hasMutation(dnaBody);

    // Saving the DNA sequence to the database
    // const dna = new DnaRecords({ sequence_dna: dnaBody.join(''), has_mutation: isMutated });
    // await dna.save();

    if (isMutated) {
        res.sendStatus(200);
    } else {
        res.sendStatus(403);
    }
}

module.exports = checkMutation;