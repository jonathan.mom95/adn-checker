const express = require('express')
const router = express.Router()
const checkMutation = require('../controllers/checkMutation');
const getRecordsStats = require('../controllers/getRecordsStats');

router.get('/', (req, res) => {
    res.writeHead(200, { 'Content-Type': 'text/html' });
    res.write('<h1>Hello from Express.js!</h1>');
    res.end();
  });

router.post('/mutation', checkMutation);

router.get('/stats', getRecordsStats);

module.exports = router;