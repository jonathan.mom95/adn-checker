
function hasMutation(sequencesDNA) {
    const n = sequencesDNA.length;

    // Verify that the matrix is square
    if (sequencesDNA.some(seq => seq.length !== n)) {
        throw new Error('The DNA sequence matrix must be square.');
    }

    // Verify that all sequences are the same length
    const lengthSequence = sequencesDNA[0].length;
    if (sequencesDNA.some(seq => seq.length !== lengthSequence)) {
        throw new Error('All DNA sequences must have the same length.');
    }

    // Traverse the matrix and search for repeated sequences.
    const sequences = [...sequencesDNA, ...transpose(sequencesDNA), ...diagonals(sequencesDNA)];

    const regex = /(AAAA|TTTT|CCCC|GGGG|aaaa|tttt|cccc|gggg)/;

    for (const sequence of sequences) {
        if (regex.test(sequence)) {
            return true;
        }
    }

    return false;
}

function transpose(matrix) {
    let [firstRow] = matrix;
    return firstRow.split('').map((_, i) => matrix.map(row => row[i]).join(''));
}

function* diagonals(matrix) {
    const n = matrix.length;
    for (let i = 0; i < n; i++) {
        let rowMatrix = matrix[i].split('');
        yield rowMatrix.slice(i)?.join('');
        if (i > 0) {
            yield rowMatrix.slice(0, n - i).join('');
            yield matrix.slice(i).map(row => row[n - i - 1]).join('');
            if (i < n - 1) {
                yield matrix.slice(0, n - i).map(row => row[i]).join('');
            }
        }
    }
}


module.exports = hasMutation;